"""
Sophie Ahlberg
University  of Oregon
CIS 322
Assignment 1
"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration 

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program
import os
import sys


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##
CAT = """
     ^ ^
   =(   )=
"""

# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"


def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))
    try :

        parts = request.split()
        log.info (parts)
            
        file = parts[parts.index("GET")+1][1:] #Add the 1 to remove the slash from the end of the sentence. This will be used to find the part that contains both of the two links given.
        log.info(file)
        
        file_path = os.path.abspath('./pageserver/credentials.ini/')
        drp = None #drp stands for docrootpath
        
        #finds the string pages in the DOCROOT in the credentials file
        #Received guidance from Univ of Oregon student Chase C. Further information on how to write function came from the from spew.py file, and website: https://www.webucator.com/how-to/how-read-file-with-python.cfm
    
        with open (file_path, 'r') as f:
            for line in f.readlines():
                line = line.strip() #Removes the beginning and the ending line spaces, and specil characters, such as TAB, or new line
                log.info(line)
                if (line.startswith("DOCROOT")): #Looking for the lines in the credentials that starts with the assigned word.
                    drp = line.index ("=")
                    drp = line [drp + 1:].strip()
                
        pagesString = drp #the destination of the string "/pages"
        
        
        
        if (("//") in file or ("~") in file or ("..") in file): #Uses a special character
            transmit (STATUS_FORBIDDEN, sock)
        
        elif not (file.endswith("trivia.html") or file.endswith("trivia.css")): #Writes anything other than the two destinations
            transmit (STATUS_NOT_FOUND, sock)
                
            
        elif file.endswith ("trivia.html") or file.endswith("trivia.css"):
            pagesdest = os.path.abspath(os.path.join(pagesString, file)) #The string is used to make an absolut path towards the folder with the given name of the string.
            transmit (STATUS_OK, sock)
            website = ""
            log.info("pagedest is {}".format(pagesdest))
            with open (pagesdest, 'r', encoding= 'utf-8') as source: # Source: spew.py. Also received guidance from Yukhe Lavinia
                for line in source:
                    website +=line
            transmit('Content-Type: text/html\n', sock)
            transmit('\n', sock)
            transmit(website + "\n", sock)
            
            log.info(website)
            hello = os.path.isfile(pagesdest)
            log.info("File is {}".format(hello))
            
            log.info("parts is {}".format(parts))
        
        
        else:
            log.info("Unhandled request: {}".format(request))
            transmit(STATUS_NOT_IMPLEMENTED, sock)
            transmit("\nI don't handle this request: {}\n".format(request), sock)
    except:
        log.info("ERROR: {}".format(sys.exc_info()[0]))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)
    finally:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))
   #if options.DOCROOT

    return options


def main():
    options = get_options()
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
